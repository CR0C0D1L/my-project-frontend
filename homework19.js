"use strict";

//Упражнение 1
let a = "$100";
let b = "300$";

let sum = parseInt(a.slice(1)) + parseInt(b.slice(0, 3));

console.log(sum);

//Упражнение 2
let message = " привет, медвед   ";

message = message[1].toUpperCase() + message.slice(2);

console.log(message);

//Упражнение 3
let age = prompt("Сколько вам лет?");

if (age < 4) {
  alert(`Вам ${age} лет и вы младенец`);
}

if (age >= 4 && age <= 11) {
  alert(`Вам ${age} лет и вы ребенок`);
}

if (age >= 12 && age <= 18) {
  alert(`Вам ${age} лет и вы подросток`);
}

if (age >= 19 && age <= 40) {
  alert(`Вам ${age} лет и вы познаёте жизнь`);
}

if (age >= 41 && age <= 80) {
  alert(`Вам ${age} лет и вы познали жизнь`);
}

if (age >= 81) {
  alert(`Вам ${age} лет и вы долгожитель`);
}

//Упражнение 4
let message2 = "Я работаю со строками как профессионал!";

let count = message2.split(" ");

console.log(count);
