"use strict";
let form = document.querySelector(".form");

let inputName = form.querySelector(".addreview__new_name");
let inputScore = form.querySelector(".addreview__new_rating");
let errorNameElem = form.querySelector(".error");
let errorName = "";

function handLeSubmit(event) {
  event.preventDefault();

  let name = inputName.value;
  let score = inputScore.value;

  if (name.length < 2) {
    errorName = "Имя не может быть короче 2-х символов";
  }
  if (name.length === 0) {
    errorName = "Вы забыли указать имя и фамилию";
  }
}
form.addEventListener("submit", handLeSumbit);
