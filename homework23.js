"use strict";

//Упражнение 1
let count = prompt("Введите число");

let intervalId = setInterval(function () {
  count -= 1;
  if (count === 0) {
    clearInterval(intervalId);
    console.log("Время вышло!");
  }
  console.log("осталось", count);
}, 1000);

//Упражнение 2
let promise = fetch("https://reqres.in/api/users");

promise
  .then(function (response) {
    return response.json();
  })
  .then(function (response) {
    let users = response.data;

    console.log(`Получили пользователей: ${users.length}`);
    users.forEach(function (user) {
      console.log(`- ${user.first_name} ${user.last_name} (${user.email})`);
    });
  });
