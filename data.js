let product = {
  name: "Iphone 13",
  color: "Blue",
  memory: 128,
  screen: 6.1,
  os: "IOS 15",
  WirelessInterfaces: "NFC,Bluetooth,Wi-Fi",
  CPU: "Apple A15 Bionic",
  weight: 173,
  oldPrice: 75990,
  newPrice: 67990,
  discount: -8,
  text: `Наша самая совершенная система двух камер.
  Особый взгляд на прочность дисплея.
  Чип, с которым всё супербыстро.
  Аккумулятор держится заметно дольше.
  iPhone 13 - сильный мира всего.
  
  Мы разработали совершенно новую схему расположения и развернули объективы на 45 градусов. Благодаря этому внутри корпуса поместилась наша лучшая система двухкамер с увеличенной матрицей широкоугольной камеры. Кроме того, мы освободилиместо для системы оптической стабилизации изображения сдвигом матрицы. И повысили скорость работы матрицы на сверхширокоугольной камере
  
  Новая сверхширокоугольная камера видит больше деталей в тёмных областях снимков. Новая широкоугольная камера улавливает на 47% больше света для болеекачественных фотографий и видео. Новая оптическая стабилизация со сдвигомматрицы обеспечит чёткие кадры даже в неустойчивом положении.
  
  Режим «Киноэффект» автоматически добавляет великолепные эффекты перемещения фокуса и изменения резкости. Просто начните запись видео. Режим «Киноэффект»будет удерживать фокус на объекте съёмки, создавая красивый эффект размытия вокруг него. Режим «Киноэффект» распознаёт, когда нужно перевести фокус на другого человека или объект, который появился в кадре. Теперь ваши видео будут смотреться как настоящее кино.`,
imageUrl = [
"./resources/image-1.webp", "./resources/image-2.webp","./resources/image-3.webp","./resources/image-4.webp","./resources/image-5.webp",
"resources/color-1.webp","resources/color-2.webp","resources/color-3.webp","resources/color-4.webp","resources/color-5.webp","resources/color-6.webp"  
],
colorString = ['красный','зеленый','розовый','голубой','белый','черный'],
memoryOzu = [128,256,512],
dostavka1 = [
  {
    name: 'Самовывоз',
    date: 'четверг, 1 сентября'
    сost: 0
  }
]
dostavka2 = [
  {
    name: 'Курьером'
    date: 'четверг, 1 сентября'
    сost: 0
  }
]
};

let review1 = {
  name: "Марк Г",
  star: 5,
  photo: "resources/review-1.jpeg",
  userExperience: "менее месяца",
  pros: `это мой первый айфон после после огромного количества телефонов на андроиде. всё плавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая,ширик тоже на высоте.`,
  cons: `к самому устройству мало имеет отношение, но перенос данных с андроида - адская вещь) а если нужно переносить фото с компа, то это только через itunes, который урезает качество фотографий исходное`,
};

let review2 = {
  name: "Валерий Коваленко",
  star: 4,
  photo: "resources/review-2.jpeg",
  userExperience: "менее месяца",
  pros: `OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго`,
  cons: `Плохая ремонтопригодность`,
};
