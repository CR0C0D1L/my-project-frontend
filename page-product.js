"use strict";

let form = document.querySelector(".form");
let input = form.querySelector(".input");

let addreview__new_nameContainer = form.querySelector(".addreview__new_name");
let addreview__new_ratingContainer = form.querySelector(
  ".addreview__new_rating"
);

let inputName = addreview__new_nameContainer.querySelector(".inputName");
let errorNameElem = addreview__new_nameContainer.querySelector(".error");
let inputScore = addreview__new_ratingContainer.querySelector(".input-score");
let errorScoreElem = addreview__new_ratingContainer.querySelector(".error");

let errorName = "";
function handLeSumbit(event) {
  event.preventDefault();

  localStorage.removeItem("first-name");
  console.log("submit");

  let name = inputName.value;
  let score = inputScore.value;

  //For name
  let errorName = "";
  if (name.length === 0) {
    errorName = "Вы забыли указать имя и фамилию";
  }
  if (name.length < 2) {
    errorName = "Имя не может быть короче 2-х символов";
  }
  errorNameElem.innerText = errorName;
  errorNameElem.classList.toggle("visible", errorName);

  //For Score
  let errorScore = "";

  if (score < 1 || score > 5) {
    errorName = "Оценка должна быть от 1 до 5";
  }
  if (errorScore) {
    errorNameScore.innerText = errorName;
    errorNameScore.classList.toggle("visible", errorScore);
  }
}
function handLeInput(event) {
  let value = event.target.value;
  let name = event.target.getAttribute("name");

  console.log(name, value);
  localStorage.setItem(name, value);
  console.log(localStorage.getitem(name));
}
input.value = localStorage.getItem("first-name");
form.addEventListener("submit", handLeSubmit);
input.addEventListener("input", handLeInput);
