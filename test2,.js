"use strict";
let button = document.querySelector(".bbut");

button.addEventListener("click", function () {
  alert("Пользователь нажал добавить в корзину!");
});

let form = document.querySelector("form");
// вызывается при отправке формы
form.addEventListener("submit", (event) => {
  // Отменяем поведение по умолчанию
  event.preventDefault();
  console.log("Делаем что угодно");
});

let input = document.querySelector(".addreview__new_name");
input.addEventListener("input", (event) => {
  console.log(event.target.value);
});
