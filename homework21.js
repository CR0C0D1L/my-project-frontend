"use strict";

//Упражнение 1
let user = {};
alert(isEmpty(user));

user.age = 12;
alert(isEmpty(user));

/** Возвращаем true если объект без свойств
 *
 * @param {number} obj объект
 * @returns
 */

function isEmpty(obj) {
  for (let key in obj) {
    return false;
  }
  return true;
}

//Упражнение 3
let salaries = {
  John: 100000,
  Ann: 160000,
  Pete: 130000,
};
/** Считаем ЗП повышенную на perzent
 *
 * @param {number} perzent процент на который повысится ЗП
 * @returns {number} newSalaries вернет ЗП умноженную на perzent
 */
function raiseSalary(perzent) {
  let newSalaries = {};

  for (let key in salaries) {
    let raise = (salaries[key] * perzent) / 100;

    newSalaries[key] = salaries[key] + raise;
  }

  return newSalaries;
}
/** Считаем сумму повышенных ЗП
 *
 * @param {number} obj сумма заплат
 * @returns {number} summ возвращает сумму зарплат
 */
function calcSumm(obj) {
  let summ = 0;

  for (let key in obj) {
    summ = summ + obj[key];
  }
  return summ;
}
let result = raiseSalary(5);
let summ = calcSumm(result);

console.log(salaries, result);
console.log(summ);
