"use strict";

//Упражнение 1
for (let a = 0; a <= 20; a++) {
  if (a % 2 == 0) {
    console.log(a);
  }
}

//Упражнение 2
let sum = 0;
let count = 0;

while (count != 3) {
  let value = +prompt("Введите число");
  sum += value;
  count++;

  if (!value) {
    alert("Ошибка, вы ввели не число!");
    break;
  }
}
alert("Сумма:" + sum);

//Упражнение 3
function getNameOfMonth(montNumber) {
  switch (montNumber) {
    case 0:
      console.log("Январь");
      break;
    case 1:
      console.log("Февраль");
      break;
    case 2:
      console.log("Март");
      break;
    case 3:
      console.log("Апрель");
      break;
    case 4:
      console.log("Май");
      break;
    case 5:
      console.log("Июнь");
      break;
    case 6:
      console.log("Июль");
      break;
    case 7:
      console.log("Август");
      break;
    case 8:
      console.log("Сентябрь");
      break;
    case 9:
      console.log("Октябрь");
      break;
    case 10:
      console.log("Ноябрь");
      break;
    case 11:
      console.log("Декабрь");
      break;
  }
}
getNameOfMonth(1);

for (let i = 0; i <= 11; i++) {
  if (i === 9) continue;
  switch (i) {
    case 0:
      console.log("Январь");
      break;
    case 1:
      console.log("Февраль");
      break;
    case 2:
      console.log("Март");
      break;
    case 3:
      console.log("Апрель");
      break;
    case 4:
      console.log("Май");
      break;
    case 5:
      console.log("Июнь");
      break;
    case 6:
      console.log("Июль");
      break;
    case 7:
      console.log("Август");
      break;
    case 8:
      console.log("Сентябрь");
      break;
    case 9:
      console.log("Октябрь");
      break;
    case 10:
      console.log("Ноябрь");
      break;
    case 11:
      console.log("Декабрь");
      break;
  }
}
