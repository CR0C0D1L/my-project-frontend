"use strict";

// Упражнение 1
let a = "100px";
let b = "323px";

let result = parseInt(a) + parseInt(b);

console.log(result);

//Упражнение 2
console.log(Math.max(10, -45, 102, 36, 12, 0, -1));

//Упражнение 3
let c = 123.3399;
Math.round(c);

console.log(Math.round(c));

let d = 0.111;
Math.ceil(d);

console.log(Math.ceil(d));

let e = 45.333333;
e.toFixed(1);

console.log(e.toFixed(1));

let f = 3;
let s = 5;
let fs = 3 ** 5;

console.log(fs);

let g = 400000000000000;
let gg = 4e14;

console.log(gg);

let h = "1" != 1; //false
let z = "1" == 1; //true

console.log(z);

//Упражнение 4
console.log(0.1 + 0.2 === 0.3); // Вернёт false, потому что значения 0.1 и 0.2 явл. бесконечной дробью в двоичной форме
